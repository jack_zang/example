package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
)

//获取一个网页所有的内容
func HttpGet4(url string) (result string, err error) {
	resp, err1 := http.Get(url)
	if err1 != nil {
		err = err1
		return
	}
	defer resp.Body.Close()
	buf := make([]byte, 4096)
	for {
		n, err2 := resp.Body.Read(buf)
		if n == 0 {
			break
		}
		if err2 != nil && err2 != io.EOF {
			err = err2
			return
		}
		result += string(buf[:n])
	}
	return
}

func SaveJoke2File(idx int, fileTitle, fileContent []string) {
	path := "D:/test/第" + strconv.Itoa(idx) + "页.txt"
	f, err := os.Create(path)
	if err != nil {
		fmt.Println("Create file err:", err)
		return
	}
	defer f.Close()
	n := len(fileTitle)
	for i := 0; i < n; i++ {
		f.WriteString(fileTitle[i] + "\n" + fileContent[i] + "\n")
		f.WriteString("----------------------------------\n")
	}
}

//抓取一个网页，带有10个段子
func SpiderPage3(idx int, page chan int) {
	//拼接url
	url := "https://www.pengfue.com/xiaohua_" + strconv.Itoa(idx) + ".html"

	//封装函数获取段子的URL
	result, err := HttpGet4(url)
	if err != nil {
		fmt.Println("HttpGet err", err)
		return
	}
	//解析，编译正则
	ret := regexp.MustCompile(`<h1 class="dp-b"><a href="(?s:(.*?))"`)
	//提取需要信息-每一个段子url
	alls := ret.FindAllStringSubmatch(result, -1)

	//创建用户存储title,content的切片
	fileTitle := make([]string, 0)
	fileContent := make([]string, 0)

	for _, jokeURL := range alls {
		//fmt.Println("jokeURL:",jokeURL[1])
		title, content, err := SpiderJokePage(jokeURL[1])
		if err != nil {
			fmt.Println("SpiderJokePage err:", err)
			continue
		}
		//fmt.Println("title",title)
		//fmt.Println("Content",content)
		fileTitle = append(fileTitle, title)
		fileContent = append(fileContent, content)

	}
	//fmt.Println("title",fileTitle)
	//fmt.Println("Content",fileContent)
	SaveJoke2File(idx, fileTitle, fileContent)

	//防止主go程提前结束
	page <- idx
}

//爬取一个页面的titm
func SpiderJokePage(url string) (title, content string, err error) {
	result, err1 := HttpGet4(url)
	if err1 != nil {
		err = err1
		return
	}
	//编译解析正则表达式----title
	ret1 := regexp.MustCompile(`<h1>(?s:(.*?))</h1>`)
	alls := ret1.FindAllStringSubmatch(result, 1) //有2处，取第一个
	for _, tmpTitle := range alls {
		title = tmpTitle[1]
		title = strings.Replace(title, "\n", "", -1)
		title = strings.Replace(title, "\t", "", -1)
		break
	}

	//编译解析正则表达式----content
	ret2 := regexp.MustCompile(`<div class="content-txt pt10">(?s:(.*?))<a id="prev" href="`)
	alls2 := ret2.FindAllStringSubmatch(result, 1) //有2处，取第一个
	for _, tmpContent := range alls2 {
		content = tmpContent[1]
		content = strings.Replace(content, "\n", "", -1)
		content = strings.Replace(content, "\t", "", -1)
		break
	}
	return

}

func toWork0401(start, end int) {
	fmt.Printf("正在爬取%d到%d页...\n", start, end)

	page := make(chan int)
	for i := start; i <= end; i++ {
		go SpiderPage3(i, page)
	}

	for i := start; i <= end; i++ {
		fmt.Println("第%d页爬取完毕\n", <-page)
	}

}

func main() {
	//指定爬取起始，终止页
	var start, end int
	fmt.Print("请输入爬取的起始页（>=1）:")
	fmt.Scan(&start)
	fmt.Print("请输入爬取的终止页（>=start）:")
	fmt.Scan(&end)

	toWork0401(start, end)
}
