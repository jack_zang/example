package main

import (
	"bufio"
	"fmt"
	"os"
)

//1. 使用Scanln，Sscanf方式读取
var (
	firstName, lastName, s string
	i                      int
	f                      float32
	input                  = "56.12 / 5212 / Go"
	format                 = "%f / %d / %s "
)

func main0101() {
	fmt.Println("Please enter your full name: ")
	fmt.Scanln(&firstName, &lastName) //让用户输入姓名
	fmt.Printf("Hi %s %s!\n", firstName, lastName)

	fmt.Sscanf(input, format, &f, &i, &s) //以指定格式打印
	fmt.Println("From the string we read: ", f, i, s)
}


//2. 使用 bufio方式
var inputReader *bufio.Reader
var input2 string
var err error

func main0102() {
	inputReader=bufio.NewReader(os.Stdin)
	fmt.Println("Please enter some input: ")
	input2,err=inputReader.ReadString('\n')
	if err==nil{
		fmt.Printf("The input was: %s\n", input2)
	}
}










