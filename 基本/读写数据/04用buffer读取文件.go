package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
)

func cat(r *bufio.Reader) {
	for {
		buf,err:=r.ReadBytes('\n')
		if err == io.EOF{
			break
		}
		fmt.Fprintf(os.Stdout, "%s",buf)
	}
	return
}

func main0401() {
	flag.Parse()
	if flag.NArg() ==0{
		cat(bufio.NewReader(os.Stdin))
	}
	for i:=0;i<flag.NArg();i++{
		f,err:=os.Open(flag.Arg(i))
		if err!=nil{
			fmt.Fprint(os.Stderr,"%s:error reading from %s: %s\n",os.Args[0],flag.Arg(i),err.Error())
			continue
		}
		cat(bufio.NewReader(f))
	}
}

//2. 使用 flag 添加一个选项，目的是为输出的每一行开头加入一个行号。
// test.exe -n gotest.txt
var numberFlag = flag.Bool("n",false,"number each line")

func cat02(r *bufio.Reader) {
	i:=1
	for {
		buf,err:=r.ReadBytes('\n')
		if err==io.EOF{
			break
		}
		if *numberFlag{
			fmt.Fprintf(os.Stdout, "%5d %s", i, buf)
			i++
		}else{
			fmt.Fprintf(os.Stdout,"%s", buf)
		}
	}
	return
}

func main() {
	flag.Parse()
	if flag.NArg() ==0{
		cat02(bufio.NewReader(os.Stdin))
	}
	for i:=0;i<flag.NArg();i++{
		f,err:=os.Open(flag.Arg(i))
		if err!=nil{
			fmt.Fprintf(os.Stderr,"%s:error reading from %s: %s\n", os.Args[0], flag.Arg(i), err.Error())
			continue
		}
		cat02(bufio.NewReader(f))
	}
}










