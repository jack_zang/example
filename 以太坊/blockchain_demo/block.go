package main

import (
	"bytes"
	"encoding/binary"
	"log"
)

//0.定义结构
type Block struct {
	//1. 版本号
	Version uint64
	//2. 前区块哈希
	PrevHash []byte
	//3. Merker根（梅克尔根，这就是一个哈希值，后面介绍）
	MerkerRoot []byte
	//4. 时间戳
	TimeStamp uint64
	//5. 难度值
	Difficulty uint64
	//6. 随机数
	Nonce uint64

	//a. 当前区块哈希，正常比特币中没有当前区块的哈希，我们为了是方便做了简化！
	Hash []byte
	//b. 数据
	Data []byte
}

//实现一个辅助函数，功能是将uint64转成[]byte
func Unit64ToByte(num uint64) []byte{
	var buffer bytes.Buffer
	err:=binary.Write(&buffer,binary.BigEndian,num)
	if err!=nil{
		log.Panic(err)
	}
	return buffer.Bytes()
}


//1.创建区块
func NewBlock(data string, prevBlockHash []byte) *Block {
	block := Block{
		Version: 00,
		PrevHash: prevBlockHash,
		MerkerRoot:[]byte{},
		TimeStamp: 0, //随便填的无效值
		Difficulty:0,  //同上
		Hash:     []byte{}, //先填空，后面再计算   //TODO
		Data:     []byte(data),
	}

	//block.SetHash()
	//创建一个pow对象
	pow:=NewProofOfWork(&block)
	hash,nonce:=pow.Run()
	//查找随机数，不停的进行哈希运行
	block.Hash=hash
	//根据挖矿结果对区块数据进行更新（补充）
	block.Nonce=nonce
	return &block
}

/*
//2.生成哈希
func (block *Block) SetHash() {
	//var blockInfo []byte
	//1. 拼装数据
	/*
	blockInfo = append(blockInfo,Unit64ToByte(block.Version)...)
	blockInfo = append(blockInfo,block.PrevHash...)
	blockInfo = append(blockInfo,block.MerkerRoot...)
	blockInfo = append(blockInfo,Unit64ToByte(block.TimeStamp)...)
	blockInfo = append(blockInfo,Unit64ToByte(block.Difficulty)...)
	blockInfo = append(blockInfo,Unit64ToByte(block.Nonce)...)
	blockInfo = append(blockInfo,block.Data...)
    */
    /*
    tmp := [][]byte{
    	Unit64ToByte(block.Version),
    	block.PrevHash,
    	block.MerkerRoot,
    	Unit64ToByte(block.TimeStamp),
    	Unit64ToByte(block.Difficulty),
    	Unit64ToByte(block.Nonce),
    	block.Data,
	}

    //将二维的切片数组连接起来,返回一个一维的切片
    blockInfo:=bytes.Join(tmp,[]byte{})

	//2. sha256
	hash := sha256.Sum256(blockInfo)
	block.Hash = hash[:]
}

*/












